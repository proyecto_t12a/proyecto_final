package cadenas_02;

import java.util.Scanner;

public class Problema_06 {

    static String [] codigos = {"6002251RATRM00Y",
                                "6204251SATRM00C",
                                "6002251RATRM005",
                                "6005251SATRM00D",
                                "6002251RATRM005"};

    public static void main(String[] args) {
        int mes, nro_personas_mes, nro_coincidencias, codigo_sin_numero;
        String apellido_paterno;
        Scanner sc = new Scanner(System.in);

        System.out.println("Ingrese el mes: ");
        mes = sc.nextInt();
        nro_personas_mes = hallar_personas_x_mes(mes);

        sc.nextLine();
        System.out.println("Ingrese el Apellido Paterno: ");
        apellido_paterno = sc.nextLine();
        nro_coincidencias = hallar_coincidencias(apellido_paterno);

        codigo_sin_numero = hallar_sin_numero();

        System.out.println("El numero de personas nacidas dicho mes es: "+ nro_personas_mes);
        System.out.println("El numero de codigos que coindicen con el apellido es: "+ nro_coincidencias);
        System.out.println("El numero de personas que no finaliza su codigo con numero es: "+ codigo_sin_numero);
    }

    static int hallar_personas_x_mes (int mes)
    {
        int cant_personas = 0;
        int mes_vector;

        for(int i = 0; i < codigos.length; i++)
        {
            mes_vector = Integer.parseInt(codigos[i].substring(2,4));

            if(mes == mes_vector)
                cant_personas++;
        }

        return cant_personas;
    }

    static int hallar_coincidencias (String apellido)
    {
        int cant_coincidencias = 0;
        String apellido_vector, codigo_apellido;

        codigo_apellido = apellido.substring(0,1) + apellido.substring(3,4);

        for(int i = 0; i < codigos.length; i++)
        {
            apellido_vector = codigos[i].substring(7,9);

            if(apellido_vector.equals(codigo_apellido))
                cant_coincidencias++;
        }

        return cant_coincidencias;
    }

    static int hallar_sin_numero()
    {
        int cant_sin_numero = 0;
        String caracter_final;

        for(int i = 0; i < codigos.length; i++)
        {
            caracter_final = codigos[i].substring(14,15);

            if( !caracter_final.equals("0") && !caracter_final.equals("1") && !caracter_final.equals("2")
             && !caracter_final.equals("3") && !caracter_final.equals("4") && !caracter_final.equals("5")
             && !caracter_final.equals("6") && !caracter_final.equals("7") && !caracter_final.equals("8")
             && !caracter_final.equals("9"))
                cant_sin_numero++;
        }

        return cant_sin_numero;
    }
}