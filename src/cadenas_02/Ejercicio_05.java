package cadenas_02;

import java.util.Scanner;

public class Ejercicio_05 {

    static String [] codigos = {"LANBCY236C21:30",
                                "AVIGTR235P23:00",
                                "LANHTY984D18:30",
                                "LANFRT345D22:30",
                                "COPZDE894D18:00"};

    public static void main(String[] args) {
        String nro_vuelo, estado_vuelo, aerolinea;
        int cant_vuelos_retrasados, cant_vuelos_salida;
        Scanner sc = new Scanner(System.in);
        int hora_inicio, hora_fin;

        /*System.out.println("Ingrese el nro de vuelo: ");
        nro_vuelo = sc.nextLine();
        estado_vuelo = obtener_estado(nro_vuelo);
        System.out.println("\nEl estado del vuelo es: " + estado_vuelo);*/

        /*System.out.println("Ingrese la aerolinea: ");
        aerolinea = sc.nextLine();
        cant_vuelos_retrasados = calcular_retrasos(aerolinea);
        System.out.println("\nEl nro de vueltos atrasados es: " + cant_vuelos_retrasados);*/

        hora_inicio = 18;
        hora_fin = 22;
        cant_vuelos_salida = calcular_vueltos_x_hora(hora_inicio, hora_fin);
        System.out.println("\nEl nro de vueltos de "+ hora_inicio + ":00 a "+ hora_fin +":00 es: " + cant_vuelos_salida);
    }

    static String obtener_estado(String vuelo)
    {
        String estado = "", nro_vuelo_vector;

        for(int i=0; i < codigos.length; i++)
        {
            nro_vuelo_vector = codigos[i].substring(3,9);
            if(nro_vuelo_vector.equals(vuelo)) {
                estado = codigos[i].substring(9, 10);
                i = codigos.length;
            }
        }

        return estado;
    }

    static int calcular_retrasos(String aerolinea)
    {
        int cant_vuelos = 0;
        String aerolinea_vector, estado;

        for(int i=0; i < codigos.length; i++)
        {
            aerolinea_vector = codigos[i].substring(0,3);
            if(aerolinea_vector.equals(aerolinea)) {
                estado = codigos[i].substring(9, 10);

                if(estado.equals("D"))
                    cant_vuelos++;
            }
        }

        return cant_vuelos;
    }

    static int calcular_vueltos_x_hora (int hora_inicio, int hora_fin)
    {
        int cant_vuelos = 0, hora_vector, minutos_vector;

        for(int i=0; i < codigos.length; i++)
        {
            hora_vector = Integer.parseInt(codigos[i].substring(10,12));
            minutos_vector = Integer.parseInt(codigos[i].substring(13,15));

            if((hora_vector >= hora_inicio && hora_vector < hora_fin)
             || (hora_vector == hora_fin && minutos_vector == 0)) {
                cant_vuelos++;
            }
        }

        return cant_vuelos;
    }
}