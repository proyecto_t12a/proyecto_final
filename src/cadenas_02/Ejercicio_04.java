package cadenas_02;

import java.util.Scanner;

public class Ejercicio_04 {

    static String [] dni = {"70342550", "00342550", "00343450", "70349750", "50342550", "70342588", "90342660"};
    static String [] nombre = {"Felipe", "Ronald", "Luisa", "Luisa", "Luisa", "Enrique", "Felipe"};
    static int [] edad = {25, 49, 35, 24, 29, 45, 42};
    static String [] sexo = {"M", "M", "F", "F", "F", "M", "M"};

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        /*int cant_hombres_40;
        cant_hombres_40 = hallar_cant_hombre_40();
        System.out.println("\nEl nro de hombres mayores a 40 es: " + cant_hombres_40);*/

        /*String dni, nombre;
        System.out.println("Ingrese el DNI buscado");
        dni = sc.nextLine();
        nombre = hallar_nombre(dni);
        System.out.println("\nEl DNI " + dni + " pertenece a " + nombre);*/

        String nombre_buscado;
        int homonimos;
        System.out.println("Ingrese el nombre a buscar");
        nombre_buscado = sc.nextLine();
        homonimos = hallar_homonimos(nombre_buscado);
        System.out.println("\nLa cantidad de homonimos es " + homonimos);
    }

    static int hallar_cant_hombre_40()
    {
        int cant_hombres_40 = 0;

        for(int i=0; i < nombre.length; i++)
        {
            if(sexo[i].equals("M") && edad[i]>40)
                cant_hombres_40++;
        }

        return cant_hombres_40;
    }

    static String hallar_nombre (String DNI)
    {
        String persona = "";

        for(int i=0; i < nombre.length; i++)
        {
            if(dni[i].equals(DNI)) {
                persona = nombre[i];
                i = nombre.length;
            }
        }

        return persona;
    }

    static int hallar_homonimos(String nombre_persona)
    {
        int cant_homonimos = 0;

        for(int i=0; i < nombre.length; i++)
        {
            if(nombre[i].equals(nombre_persona)) {
                cant_homonimos++;
            }
        }

        cant_homonimos--;

        return cant_homonimos;
    }
}