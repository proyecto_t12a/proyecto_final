package cadenas_02;

import java.util.Scanner;

public class Problema_05 {

    static String [] codigos = {"LANBCY236C21:30",
                                "AVIGTR235P21:00",
                                "LANHTY984D18:30",
                                "LANFRT345D22:30",
                                "COPZDE894D18:00"};

    // LAN          : LAN
    // COPA AIRLINES: COP
    // AVIANCA      : AVI

    public static void main(String[] args) {

        String codigo_vuelo, estado_vuelo, aerolinea;
        int vuelos_atrasados, vueltos_desde_18;
        Scanner sc = new Scanner(System.in);

        System.out.println("Indique el codigo de vuelo");
        codigo_vuelo = sc.nextLine();

        System.out.println("Indique el nombre de la aerolinea");
        aerolinea = sc.nextLine();

        estado_vuelo = obtenerEstadoVuelo(codigo_vuelo);
        vuelos_atrasados = encontrar_vuelos_atrasados(aerolinea);
        vueltos_desde_18 = encontrar_vuelos_x_horas(18, 19);

        System.out.println("\nEl estado del vuelo es: " + estado_vuelo);
        System.out.println("\nLa cantidad de vuelos atrasados de " + aerolinea + " es: " + vuelos_atrasados);
        System.out.println("\nLa cantidad de vuelos entre las 18:00 y 19:00 es: " + vueltos_desde_18);
    }

    static String obtenerEstadoVuelo(String codigo)
    {
        String estado = "";
        String codigo_vector;

        for(int i=0; i < codigos.length; i++)
        {
            codigo_vector = codigos[i].substring(3,9);

            if(codigo_vector.equals(codigo))
            {
                estado = codigos[i].substring(9,10);
                i = codigos.length;
            }
        }

        return estado;
    }

    static int encontrar_vuelos_atrasados(String aerolinea)
    {
        int vuelos_atrasados = 0;
        String codigo_aerolinea, estado;

        for(int i=0; i < codigos.length; i++)
        {
            codigo_aerolinea = codigos[i].substring(0,3);

            if(codigo_aerolinea.equals(aerolinea))
            {
                estado = codigos[i].substring(9,10);

                if(estado.equals("D"))
                    vuelos_atrasados++;
            }
        }

        return vuelos_atrasados;
    }

    static int encontrar_vuelos_x_horas(int hora_inicio, int hora_fin)
    {
        int vuelos_despues = 0;
        int hora_vuelo, minutos_vuelo;

        for(int i=0; i < codigos.length; i++)
        {
            hora_vuelo = Integer.parseInt(codigos[i].substring(10,12));
            minutos_vuelo = Integer.parseInt(codigos[i].substring(13,15));

            if(hora_vuelo == hora_inicio || (hora_vuelo == hora_fin && minutos_vuelo == 0))
                vuelos_despues++;
        }

        return vuelos_despues;
    }
}
