package codigos;

import java.util.Scanner;

public class Cadenas_05 {

    static String [] basedes = {"EL","FA","CP","IR","RS","PG"};
    static String [] candidatos = {"KF","PK","CA","AG","JG","AT"};
    static String [] resultados= {"KF","KF","KF","KF","KF","KF",
                                    "PK","PK","PK","PK","PK","PK",
                                    "CA","CA","CA","CA","CA","CA",
                                    "AG","AG","AG","AG","AG","AG",
                                    "JG","JG","JG","JG","JG","JG",
                                    "AT","AT","AT","AT","AT","AT"};

    static String [] hojavida= {"EL","FA","CP","IR","RS","PG",
                                "EL","FA","CP","IR","RS","PG",
                                "EL","FA","CP","IR","RS","PG",
                                "EL","FA","CP","IR","RS","PG",
                                "EL","FA","CP","IR","RS","PG",
                                "EL","FA","CP","IR","RS","PG"};

    static int [] puntaje = {350,122,431,101,341,78,
                            450,121,452,123,111,345,
                            123,123,431,124,153,231,
                            321,145,651,138,765,132,
                            333,111,222,444,555,123,
                            451,453,431,467,454,344};

/*    static int [] puntaje = {150,122,431,801,341,780,
                            250,121,452,123,111,345,
                            523,123,451,124,153,231,
                            921,945,951,938,765,132,
                            433,311,262,444,555,123,
                            651,453,431,467,454,344};

    static int [] puntaje = {350,122,431,901,941,978,
                            450,121,452,123,111,345,
                            423,123,431,124,153,231,
                            321,105,151,138,765,132,
                            333,111,122,444,555,123,
                            401,403,431,467,454,344};*/

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Ingrese un candidato: ");
        String candidato = sc.nextLine();

        int puntaje = obtenerPuntajeCandidato(candidato);
        System.out.printf("Para el candidato %s el puntaje obtenido es: %d\n",candidato,puntaje);

        String candMayor = calcularmayorpuntaje();
        System.out.println("El candidato con mayor puntaje es: " + candMayor);

        String mayorCV = test_calcularmayorhojavida();
        System.out.println("La descripción de la hoja de vida que obtuvo el mayor puntaje de entre los candidatos es: "+mayorCV);
    }

    private static int obtenerPuntajeCandidato(String candidato) {
        int puntajetotal=0;
        for (int i=0; i<resultados.length;i++){
            if (resultados[i].equals(candidato)){
                puntajetotal = puntajetotal + puntaje[i];
            }
        }
        return puntajetotal;
    }

    private static String calcularmayorpuntaje() {
        int mayor = 0;
        int indice = 0;
        int totalcan = 0;

        for (int i=0;i<candidatos.length;i++){
            totalcan = obtenerPuntajeCandidato(candidatos[i]);
            if (totalcan > mayor){
                mayor = totalcan;
                indice = i;
            }
        }
        return candidatos[indice];
    }

    private static String test_calcularmayorhojavida() {
        int res1 = 0;
        int res2 = 0;
        int res3 = 0;
        int res4 = 0;
        int res5 = 0;
        int res6 = 0;

        for (int i=0;i<resultados.length;i++){
            if (hojavida[i].equals("EL")){
                res1 += puntaje[i];
            }else if (hojavida[i].equals("FA")) {
                res2 += puntaje[i];
            }else if (hojavida[i].equals("CP")) {
                res3 += puntaje[i];
            }else if (hojavida[i].equals("IR")) {
                res4 += puntaje[i];
            }else if (hojavida[i].equals("RS")) {
                res5 += puntaje[i];
            }else if (hojavida[i].equals("PG")) {
                res6 += puntaje[i];
            }
        }
        int [] parciales = {res1,res2,res3,res4,res5,res6};
        int mayor = parciales[0];
        int indice = 0;
        for (int i=0;i<parciales.length;i++){
            if (parciales[i]>mayor){
                mayor = parciales[i];
                indice = i;
            }
        }
        return basedes[indice];
    }
}
