package codigos;

import java.util.Scanner;

public class Cadenas_01 {

    static String [] codigos = {"AR000101","PE125610","CH452315","BR521317","BR123620","PE752124","AR124511","PE562405","PE562405","BR452102","AR455210"};
    //static String [] codigos = {"PE521105","PE124506","BR456607","BR454508","BR784610","CH578712","CH784514","CH823516","CH112318","PE122120","AR475522"};
    //static String [] codigos = {"BR002230","PE025628","BR014526","PE123524","BR326522","PE002520","CH525118","CH521416","CH021414","AR450112","CH465310"};

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Ingrese la nacionalidad: ");
        String nacionalidad =sc.nextLine();

        int cantidad = obtenerCantidadProductos(nacionalidad);
        System.out.printf("Para la nacionalidad %s, la cantidad de productos es %d\n",nacionalidad,cantidad);

        int correlativo = obtenerUltimoCorrelativo(nacionalidad);
        System.out.printf("Para la nacionalidad %s, el último correlativo es %d\n",nacionalidad,correlativo);

        int costo = costoTotalAlmacenaje(nacionalidad);
        System.out.printf("Para la nacionalidad %s, el costo de almacenaje es %d\n",nacionalidad,costo);
    }

    private static int obtenerCantidadProductos(String nacionalidad) {
        int contador = 0;

        for (int i=0; i<codigos.length; i++){
            if (codigos[i].substring(0,2).equals(nacionalidad)){
                contador += 1;
            }
        }
        return contador;
    }

    private static int obtenerUltimoCorrelativo(String nacionalidad) {
        int correlativo = 0;

        for (int i=0; i<codigos.length; i++){
            if (codigos[i].substring(0, 2).equals(nacionalidad) &&
                    Integer.parseInt(codigos[i].substring(2, 6)) > correlativo) {
                correlativo = Integer.parseInt(codigos[i].substring(2, 6));
            }
        }
        return correlativo;
    }

    private static int costoTotalAlmacenaje(String nacionalidad) {
        int costo = 0;

        for (int i=0; i<codigos.length; i++){
            if (codigos[i].substring(0,2).equals(nacionalidad)){
                costo += Integer.parseInt(codigos[i].substring(6, 8));
            }
        }
        return costo;
    }
}
