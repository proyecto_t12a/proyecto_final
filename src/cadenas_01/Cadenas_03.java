package codigos;

import java.util.Arrays;
import java.util.Scanner;

public class Cadenas_03 {
    static String [] placas = {"AGX123","BGX837","IUR923","YRS623","PSO998","MWR836"};
    static String [] modelos = {"Camion","Auto","Auto","Auto","Auto","Auto"};

    //static String [] placas = {"RYZ930","GUE844","CHI934","ROX123","WWW333","POR923"};
    //static String [] modelos = {"Bus","Camion","Bus","Bus","Bus","Bus"};
    //static String [] placas = {"AGX125","BGX838","IUR929","YRS611","PSO977","MWR845"};
    //static String [] modelos = {"Auto","Bus","Camion","Bus","Camion","Camion"};

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingresar el dígito a buscar en placa: ");
        int digito = sc.nextInt();

        int cantVehiculos = cantidadVehiXDigito(digito);
        System.out.printf("La cantidad de vehiculos para el digito %d es %d\n",digito,cantVehiculos);

        String tipoVehiculo = tipoVehiculoMenosFrec();
        System.out.println("El tipo de vehículo que transita con menor frecuencia es: " + tipoVehiculo);

        String [] listaPlacas = listarPlacasVerdeRojo();
        System.out.println("Las placas con sticker verde o rojo son: " + Arrays.toString(listaPlacas));
    }

    private static int cantidadVehiXDigito(int digito) {
        int cont = 0;
        int ultimoDigito = 0;
        for (int i=0; i<placas.length; i++){
            ultimoDigito = Integer.parseInt(placas[i].substring(5,6));
            if (ultimoDigito == digito){
                cont += 1;
            }
        }
        return cont;
    }
    private static String tipoVehiculoMenosFrec() {
        int contCamion = 0;
        int contBus = 0;
        int contAuto = 0;
        String tipoVehiculo = "";

        for (int i=0; i<placas.length; i++){
            if (modelos[i].equals("Camion")){
                contCamion += 1;
            }else if (modelos[i].equals("Auto")){
                contAuto += 1;
            }else if (modelos[i].equals("Bus")){
                contBus += 1;
            }
        }

        if (contAuto>contCamion && contBus>contCamion){
            tipoVehiculo = "Camion";
        }else if(contBus>contAuto && contCamion>contAuto){
            tipoVehiculo = "Auto";
        }else if(contAuto>contBus && contCamion>contBus){
            tipoVehiculo = "Bus";
        }
        return tipoVehiculo;
    }

    private static String[] listarPlacasVerdeRojo() {
        String [] respuestas = new String[placas.length];
        int digito = 0;
        int contador = 0;

        for (int i=0; i<placas.length; i++){
            digito = Integer.parseInt(placas[i].substring(5,6));
            if (digito >= 5 && digito <=8){
                respuestas[contador] = placas[i];
                contador += 1;
            }
        }

        //Para que solo devuelva un arreglo sin valores nulos
/*        String [] arrF = new String[contador];
        System.arraycopy(respuestas, 0, arrF, 0, arrF.length);
        return arrF;*/
        return respuestas;
    }
}
