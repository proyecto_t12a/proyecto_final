package codigos;

import java.util.Scanner;

public class Cadenas_02 {

    static String []codigos = {"MO00100614520170013", "SI12011519220140012", "SM05122514520162573", "VI01061917120127351", "SM02053210220180312", "MO08041908820169815"};
    //String []codigos = {"MO05041634020153511", "SI11072521820121212", "SM09041237120162773", "VI06092537220127241", "SM12121046120180412", "MO02043212820179815"};
    //String []codigos = {"MO11071212820133413", "SI05110531520100315", "SM12112129920162523", "VI09122247120121341", "SM04081412720181312", "MO12072642820168815"};
    //String []codigos = {"MO03112141120180012", "SI13041651220151212", "SM00003212620161573", "VI11012842120117341", "SM09062530720180322", "MO01112323820169915"};

    static String codigo = "MO00100614520170013";
    //String codigo = "SI12011537220140012";
    //String codigo = "SM05122560820162573";

    public static void main(String[] args) {
        int cantTipoJugador = cantidadTipoJugador();
        System.out.println("La cantidad de figuritas de tipo jugador es: " + cantTipoJugador);

        Scanner sc = new Scanner(System.in);
        System.out.println("Ingresar la sede: ");
        String sede = sc.nextLine();

        int cantidad = cantidadAlbumesXSede(sede);
        System.out.println("La cantidad de albumes por sede es: " + cantidad);

        String codigoMayor = mayorCantidadFiguritas(sede);
        System.out.println("El código del alumno con mayor candidad de figuritas es: " + codigoMayor);

        int numAlbumes = cantidadAlbumesMenor250();
        System.out.println("La cantidad de albumes que tienen menos de 250 figuritas es: " + numAlbumes);
    }

    private static int cantidadTipoJugador() {
        String resultado = codigo.substring(8,11);
        return Integer.parseInt(resultado);
    }

    private static int cantidadAlbumesXSede(String sede) {
        int cont = 0;
        String sedeAlbum = "";
        for (int i=0; i<codigos.length; i++){
            sedeAlbum = codigos[i].substring(0,2);
            if (sede.equals(sedeAlbum)){
                cont += 1;
            }
        }
        return cont;
    }

    private static String mayorCantidadFiguritas(String sede) {
        String codAlumno = "";
        int maximo = 0;
        String sedeAlbum = "";
        int cantidad = 0;

        for (int i=0; i<codigos.length; i++){
            sedeAlbum = codigos[i].substring(0,2);
            if (sede.equals(sedeAlbum)){
                cantidad = Integer.parseInt(codigos[i].substring(2,4)) +
                        Integer.parseInt(codigos[i].substring(4,6)) +
                        Integer.parseInt(codigos[i].substring(6,8)) +
                        Integer.parseInt(codigos[i].substring(8,11));
                if (cantidad > maximo){
                    codAlumno = codigos[i].substring(11,19);
                    maximo = cantidad;
                }
            }
        }
        return codAlumno;
    }

    private static int cantidadAlbumesMenor250() {
        int cont = 0;
        int cantidad = 0;
        for (int i=0; i<codigos.length; i++){
            cantidad = Integer.parseInt(codigos[i].substring(2,4)) +
                    Integer.parseInt(codigos[i].substring(4,6)) +
                    Integer.parseInt(codigos[i].substring(6,8)) +
                    Integer.parseInt(codigos[i].substring(8,11));
            if (cantidad < 250){
                cont += 1;
            }
        }
        return cont;
    }
}