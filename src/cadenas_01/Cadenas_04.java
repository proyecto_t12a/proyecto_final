package codigos;

import java.util.Scanner;

public class Cadenas_04 {

    static String [] listaCodigos = {"AVIAV1223D","LANLA0342T","AARAA0034D","COPCP0234D","ASDF123456"};
    static int [] montosDescuento = {300,900,1200,400,100};

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Indicar el tipo de entrada: ");
        String tipoEntrada = sc.nextLine();

        System.out.println("indicar el código promocional: ");
        String codPromocional = sc.nextLine();

        int montoEnt = obtenerMontoEntrada(tipoEntrada);
        System.out.println("El monto a pagar es: " + montoEnt);

        boolean valido = validarCodigo(codPromocional);
        System.out.println("El código promocional es: " + valido);

        int montoDesc = obtenerMontoDescuentoCodigo(codPromocional);
        System.out.println("El monto de descuento es: " + montoDesc);

        int montoPagar = obtenerMontoPagar(codPromocional,tipoEntrada);
        System.out.println("El monot a pagar es: " + montoPagar);
    }

    private static int obtenerMontoEntrada(String tipoEntrada) {
        int monto = 0;
        if (tipoEntrada.equals("normal")){
            monto = 700;
        }else if(tipoEntrada.equals("silve")){
            monto = 1700;
        }else if(tipoEntrada.equals("gold")){
            monto = 2700;
        }
        return monto;
    }

    private static boolean validarCodigo(String codPromocional) {

        if(codPromocional.length() != 10){
            return false;
        }

        for (int i=0; i<listaCodigos.length; i++){
            if(codPromocional.equals(listaCodigos[i])){
                return true;
            }
        }
        return false;
    }

    private static int obtenerMontoDescuentoCodigo(String codPromocional) {

        boolean esValido = validarCodigo(codPromocional);

        if(!esValido){
            return 0;
        }

        for (int i=0; i<listaCodigos.length; i++){
            if(codPromocional.equals(listaCodigos[i])){
                return montosDescuento[i];
            }
        }
        return 0;
    }

    private static int obtenerMontoPagar(String codPromocional, String tipoEntrada) {

        int monto = obtenerMontoEntrada(tipoEntrada);
        int descuento = obtenerMontoDescuentoCodigo(codPromocional);
        int aPagar = monto-descuento;

        if(aPagar<0){
            return 0;
        }else {
            return aPagar;
        }
    }
}